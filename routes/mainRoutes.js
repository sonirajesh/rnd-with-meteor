// Meteor.subscribe("books");
// Meteor.subscribe("Players");
Router.route('/', function () {
  this.render('home');
  SEO.set({ title: 'Home -' + Meteor.App.NAME });
});

Router.route('/index', function () {
  this.render('home');
  SEO.set({ title: 'Home -' + Meteor.App.NAME });
});

Router.route('/managebooks', function () {
  this.render('managebooks',{ data: { books: books, bookLists: books.find() }});
  SEO.set({ title: 'Books -' + Meteor.App.NAME });
});

Router.route('/leaderboard', function () {
  console.log("Players count in router = "+ Players.find().count());
  this.render('leaderboard',{data:{players: Players.find({}, {sort: {score: -1, name: 1}}) }});
  SEO.set({ title: 'Leaderboard -' + Meteor.App.NAME });
});

Router.route('/contact', function () {
  this.render('contact');
  SEO.set({ title: 'contact -' + Meteor.App.NAME });
});

Router.route('/map', function () {
  this.render('map');
  SEO.set({ title: 'Map -' + Meteor.App.NAME });
});

Router.route('/tables', function () {
  this.render('table');
  SEO.set({ title: 'Table -' + Meteor.App.NAME });
});

Router.route('/forms', function () {
  this.render('forms');
  SEO.set({ title: 'forms -' + Meteor.App.NAME });
});

Router.route('/login', function () {
  this.render('login');
  SEO.set({ title: 'Table -' + Meteor.App.NAME });
});

Router.route('/notifications', function () {
  this.render('notifications');
  SEO.set({ title: 'notifications -' + Meteor.App.NAME });
});

Router.route('/blank', function () {
  this.render('blank');
  SEO.set({ title: 'blank -' + Meteor.App.NAME });
});

Router.route('/panels-wells', function () {
  this.render('panels-wells');
  SEO.set({ title: 'panels-wells -' + Meteor.App.NAME });
});

Router.route('/buttons', function () {
  this.render('buttons');
  SEO.set({ title: 'buttons -' + Meteor.App.NAME });
});

Router.route('/pagination', function () {
  this.render('pagination');
  SEO.set({ title: 'pagination -' + Meteor.App.NAME });
});

Router.route('/typography', function () {
  this.render('typography');
  SEO.set({ title: 'typography -' + Meteor.App.NAME });
});
