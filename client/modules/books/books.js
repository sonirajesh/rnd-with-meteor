Meteor.subscribe("bookLists");
Template['managebooks'].helpers({
  selectedBookDoc: function () {
    console.log(Session.get("selectedBookId"));
    return books.findOne(Session.get("selectedBookId"));
  },
  isSelectedBook: function () {
    return Session.equals("selectedBookId", this._id);
  },
  formType: function () {
    if (Session.get("selectedBookId")) {
      return "update";
    } else {
      return "insert";
    }
  }

});
Template['reactiveListSample'].helpers({
    booklists : function () {
      return books.find();
    }
});

Template['managebooks'].events({
  'click .bookstable tr': function () {
    console.log(this._id);
    Session.set("selectedBookId", this._id);
  }
});


dataTableData = function () {
    console.log("Books count = "+ books.find().count());
    return books.find().fetch(); // or .map()
};

Template["containsTheDataTable"].helpers({
    reactiveDataFunction: function () {
        return dataTableData;
    },
    optionsObject: optionsObject // see below
});


var optionsObject = {
    columns: [{
        title: 'Title',
        data: 'title', // note: access nested data like this
        className: 'nameColumn'
    }]
}
