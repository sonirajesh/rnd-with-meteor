Meteor.subscribe("playerLists");

Template.leaderboardlist.helpers({
  players : function () {
    return Players.find();
  },
  tableSettings : function () {
    return {
        fields: [
          { key: 'name', label: 'Full Name' },
          { key: 'name', label: 'First Name', fn: function (name) { return name ? name.split(' ')[0] : ''; } },
          { key: 'score', label: 'Score' },
          { key: 'edit', label: 'Operation', fn: function () { return new Spacebars.SafeString('<button type="button" class="editbtn"><i class="fa fa-check"></i></button> <button type="button" class="deletebtn"><i class="fa fa-remove"></i></button>') } }
        ]
    };
  }
  
});
Template.leaderboardlist.events({
  'dblclick .playerstable tbody tr': function () {
    Session.set("selectedPlayerId", this._id);
    Session.set("showFormDialog", true);
  },
  'click .addPlayerBtn': function  () {
    Session.set("selectedPlayerId",null);
    Session.set("showFormDialog", true);
  },
  'click .playerstable tbody tr': function (e) {
    e.preventDefault();
    if (e.target.className == "deletebtn") {
      Players.remove(this._id)
    }
    if (e.target.className == "editbtn") {
      Session.set("selectedPlayerId", this._id);
      Session.set("showFormDialog", true);
    }
  }
});
Template.PlayersForm.helpers({
  selectedPlayerDoc: function () {
    return Players.findOne(Session.get("selectedPlayerId"));
  },
  isSelectedPlayer: function () {
    return Session.equals("selectedPlayerId", this._id);
  },
  formType: function () {
    return (Session.get("selectedPlayerId")) ? "update" : "insert";
  },
  isUpdate: function () {
    return (Session.get("selectedPlayerId")) ? true : false;
  }
});

Template.PlayersForm.events({
  'click .remove':function (evt,tmpl) {
    Players.remove(Session.get("selectedPlayerId"));
    Session.set("selectedPlayerId",null);
    Session.set("showFormDialog", false);
  },
  'click .closePlayerForm': function  () {
    Session.set("showFormDialog", false);
  }
});

Template.leaderboard.showPlayerDialog = function  () {
  return Session.get("showFormDialog");
}

AutoForm.hooks({
  PlayersFormDemo: {
   endSubmit: function() {
      Session.set("selectedPlayerId",null);
      Session.set("showFormDialog",null);
    }
  }
});

