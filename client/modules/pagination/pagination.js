Kittens = new Meteor.Collection("kittens");
KittensView = new Meteor.Collection("kittensView");
Puppies = new Meteor.Collection("puppies");

if (Meteor.isClient) {

  Template.kittens_template.helpers({
  	kitten: function() {
    	return Kittens.find();
  	}
  });

  Template.kittens_view_template.helpers({
  	kitten: function() {
    	return KittensView.find();
  	}
  });

  Template.puppies_template.helpers({
  	puppy: function() {
    	return Puppies.find();
  	}
  });

  function _pager(templ, num, handle) {
    var p = new Meteor.Paginator({
        templates: {
          content: templ + "_template"
        }
        , pagination: {
            resultsPerPage: 5 //default limit
          }
          , callbacks: {
            onPagingCompleted: function(skip, limit) {
                Session.set("pagingSkip_" + num, skip);
                Session.set("pagingLimit_" + num, limit);
                console.log("setting " + num);
              }
            , getDependentSubscriptionsHandles: function() {
                return [handle];
              }
            , getTotalRecords: function(cb) {
              //you need to return the total record count here
              //using the provided callback
              Meteor.call("totalCount", templ, function(err, result) {
                cb(result);
              });
            }
            , onTemplateRendered: function() {
              //regular render code
            }
            , onTemplateCreated: function() {
              Session.set("pagingSkip_" + num, 0);
              Session.set("pagingLimit_" + num, 5);
            }
        }
    });

    return p;
  }

  Template.kittens_parent.created = function() {
    Deps.autorun(function() {
      Meteor.subHandle1 = Meteor.subscribe("kittens", Session.get("pagingSkip_1"), Session.get("pagingLimit_1"));
    });
    Meteor.Pager1 = _pager("kittens", 1, Meteor.subHandle1);
  };

  Template.puppies_parent.created = function() {
    Deps.autorun(function() {
      Meteor.subHandle2 = Meteor.subscribe("puppies", Session.get("pagingSkip_2"), Session.get("pagingLimit_2"));
    });
    Meteor.pager2 = _pager("puppies", 2, Meteor.subHandle2);
  };

  Template.kittens_view_parent.created = function() {
    Deps.autorun(function() {
      Meteor.subHandle3 = Meteor.subscribe("kittens2", Session.get("pagingSkip_3"), Session.get("pagingLimit_3"));
    });
    Meteor.pager3 = _pager("kittens_view", 3, Meteor.subHandle3);
  };
}

if (Meteor.isServer) {

  Meteor.startup(function () {
    if (Kittens.find().count() === 0) {
      _.times(500, function(n) {
        Kittens.insert({ number: n, name: Random.hexString(5), toy: Random.hexString(10) })
      });
    }

    if (Puppies.find().count() === 0) {
      _.times(500, function(n) {
        Puppies.insert({ number: n, name: Random.hexString(7), toy: Random.hexString(10) })
      });
    }
  });

  Meteor.publish("kittens", function(skip, limit) {
    return Kittens.find({}, {
      skip: skip || 0
      , limit: limit || 10
    });
  });

  Meteor.publish("kittens2", function(skip, limit) {

    var self = this, skip = skip || 0, limit = limit || 10;

    self.ready();

    var _watchKittens = Kittens.find({}, {skip: skip, limit: limit}).observe({
      added: function(doc) {
        self.added("kittensView", doc._id, doc);
      }
      , changed: function(doc) {
         console.log("changed ", init);
        self.changed("kittensView", doc._id, doc);
      }
      , removed: function(doc) {
        self.removed("kittensView", doc._id, doc);
      }
    });

    self.onStop(function () {
        _watchKittens.stop();
    });

  });

  Meteor.publish("puppies", function(skip, limit) {
    return Puppies.find({}, {
      skip: skip || 0
      , limit: limit || 10
    });
  });

  Meteor.methods({
    totalCount: function(templ) {
      switch (templ) {
        case "kittens":
        case "kittens_view":
          console.log("total", templ);
          return Kittens.find().count();
        case "puppies":
          return Puppies.find().count();
      }
    }
  });
}