// var perPage = 10;

Deps.autorun(function() {
  Meteor.subHandle = Meteor.subscribe("allContacts", Session.get("pagingSkip"), Session.get("pagingLimit"));
});

Template['contact'].helpers({
});
Template["contactLists"].helpers({
    cs: function(){
    	return Contact.find();
    }
});
Template['contact'].events({
});

var _pager = new Meteor.Paginator({
    templates: {
        content: "contactLists"
    }, 
    pagination: {
        resultsPerPage: 10 //default limit
    }, 
    callbacks: {
        onPagingCompleted: function(skip, limit) {
            Session.set("pagingSkip", skip);
            Session.set("pagingLimit", limit);
        }, 
        getDependentSubscriptionsHandles: function() {
          return [Meteor.subHandle];
        }, 
        getTotalRecords: function(cb) {
          Meteor.call("totalCount", function(err, result) {
            cb(result);
          });
        }, 
        onTemplateRendered: function() {
            //regular render code
        }, 
        onTemplateCreated: function() {
            Session.set("pagingSkip", 0);
            Session.set("pagingLimit", 10);
        }
    }
});
