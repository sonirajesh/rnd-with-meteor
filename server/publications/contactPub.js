// if (Meteor.isServer){

Meteor.publish('allContacts', function(skip, limit) {
    check(skip, Match.Any);
    check(limit, Match.Any);
    return Contact.find({}, {
        skip: skip || 0,
        limit: limit || 10
    });
});



Meteor.methods({
  totalCount: function() {
    return Contact.find().count();
  },
  multipleContactInsert: function() {
    _.times(500, function(i) {
      Contact.insert({name:Random.hexString(10),email:Random.hexString(10)+"@mail.com",enable:true})
    });
  },
  removeAllContacts:function() {
      Contact.remove({});
  },
  sendEmail: function(doc) {
    // Important server-side check for security and data integrity
    check(doc, Schema.contact);

    // Build the e-mail text
    var text = "Name: " + doc.name + "\n\n"
            + "Email: " + doc.email + "\n\n\n\n"
            + doc.message;

    this.unblock();

    // Send the e-mail
    Email.send({
        to: "test@example.com",
        from: doc.email,
        subject: "Website Contact Form - Message From " + doc.name,
        text: text
    });
  }
});