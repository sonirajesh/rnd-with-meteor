BrowserPolicy.content.allowOriginForAll("*.googleapis.com");
BrowserPolicy.content.allowOriginForAll("*.gstatic.com");
BrowserPolicy.content.allowOriginForAll("*.bootstrapcdn.com");

BrowserPolicy.content.allowFontDataUrl();
process.env.MONGO_URL="mongodb://localhost:27017/rnd";
