Players = new Meteor.Collection("players");

Players.attachSchema(new SimpleSchema({
  name: {
    type: String,
    label: "Name",
    max: 200
  },
  score: {
    type: Number,
    label: "Score",
    min: 0
  }
}));

// Collection2 already does schema checking
// Add custom permission rules if needed
Players.allow({
    insert : function () {
      return true;
    },
    update : function () {
      return true;
    },
    remove : function () {
      return true;
    }
  });
console.log("Players count in collections = "+ Players.find().count());