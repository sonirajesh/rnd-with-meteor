Contact = new Mongo.Collection('contact');
console.log("Contact count in collections = "+ Contact.find().count());
Contact.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "Your name",
        max: 50
    },
    email: {
        type: String,
        label: "E-mail address"
    },
    enable:{
        type: Boolean,
        label: "Enable",
        optional: true
    },
    message: {
        type: String,
        label: "Message",
        max: 1000,
        optional: true
    }
}));



// Collection2 already does schema checking
// Add custom permission rules if needed
if (Meteor.isServer) {
  Contact.allow({
    insert : function () {
      return true;
    },
    update : function () {
      return true;
    },
    remove : function () {
      return true;
    }
  });
}
