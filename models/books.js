books = new Meteor.Collection('BookCollections');
books.attachSchema(new SimpleSchema({
  title: {
    type: String,
    label: "Title",
    max: 200
  },
  author: {
    type: String,
    label: "Author"
  },
  copies: {
    type: Number,
    label: "Number of copies",
    min: 0
  },
  lastCheckedOut: {
    type: Date,
    label: "Last date this book was checked out",
    optional: true
  },
  agree: {
      type: Boolean,
      label: "Do you agree?",
      autoform: {
         type: "boolean-select",
         trueLabel: "Yes, I agree",
         falseLabel: "No, I do NOT agree",
         firstOption: "(Please Choose a Response)"
      }
  },
  summary: {
    type: String,
    label: "Brief summary",
    optional: true,
    max: 1000
  }
}));

// Collection2 already does schema checking
// Add custom permission rules if needed
// if (Meteor.isServer) {
  books.allow({
    insert : function () {
      return true;
    },
    update : function () {
      return true;
    },
    remove : function () {
      return true;
    }
  });
// }
